## Abstract Balance Study

### How to run

- Download [Processing](https://processing.org/download)
- Open abstract-balance-study.pde
- Run!

You'll have a new .svg file and a new .json file in your sketchbook directory
