// Processing version 4.0b1

import processing.svg.*;
import java.util.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import java.nio.file.Files;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

int pointSize;
boolean LOG = false;

// analytics
int triesPosition = 0;
int triesColor = 0;

int currentDen = 0;

String timestamp = String.valueOf(new Date().getTime());

void settings(){
  size(600, 600, SVG, "balanced_np-"+timestamp+".svg");  
}

void setup(){
  List<Integer> dens = Arrays.asList(3,4,5,6,7,8,9,10,11,12);
  Collections.shuffle(dens);
  currentDen = dens.get(0);
  pointSize = width / currentDen;
  noStroke();
}

class Circle implements Comparable{
  public int x;
  public int y;
  public color c;
  public Circle(int x, int y, color c){
    this.x = x;
    this.y= y;
    this.c = c;
  }
  
  public int compareTo(Object o){
    if(o instanceof Circle){
      if (((Circle)o).x == this.x && ((Circle)o).y == this.y){
        return 0;
      } 
      return -1;
    }
    return -1;
  }
  
  public boolean equals(Object o){
    return o instanceof Circle && ((Circle)o).x == this.x && ((Circle)o).y == this.y;
  }
}

int getCoord(int x){
  return (x - (width)/2);
}

void draw(){
  fill(0);
  ArrayList<Circle> circles = new ArrayList<Circle>();
  int weightX = 0;
  int weightY = 0;
  int weightColor = 0;
  int epsilon = 0;
  int epsilonColor = 0;
  while(circles.isEmpty() || abs(weightX) > epsilon || abs(weightY) > epsilon){
    triesPosition++;
    circles = new ArrayList<Circle>();
    weightX = 0;
    weightY = 0;
    weightColor = 0;
    int startX = floor(pointSize);
    int startY = floor(pointSize);
    int endX = width - floor(pointSize);
    int endY = width - floor(pointSize);
    float fillThreshold = 0.5;
    float skewFactor = 1;
    for (int x = startX; x <= endX; x+=pointSize*skewFactor) {
      for (int y = startY; y <= endY; y+=pointSize*skewFactor) {
        float fillProb = random(1);
        if(fillProb > fillThreshold){
            circles.add(new Circle(x,y,color(255,255,255)));
            weightX += getCoord(x);
            weightY += getCoord(y);
        }  
      }
    }
    println("final x weight: "+weightX);
    println("final y weight: "+weightY);
  }
  
  println("Assigning colors...");
  color bg;
  do{
    triesColor++;
    int rbg = floor(random(1)*(255));
    int gbg = floor(random(1)*(255));
    int bbg = floor(random(1)*(255));
    bg = color(rbg, gbg, bbg);
    weightColor += (sqrt(pow(rbg,2)+pow(gbg,2)+pow(bbg,2)) - sqrt(pow(127,2)+pow(127,2)+pow(127,2)));
    weightColor = 0;
    for(Circle c: circles){
      int r = floor(random(1)*(255));
      int g = floor(random(1)*(255));
      int b = floor(random(1)*(255));
      color col = color(r, g, b);
      weightColor += (sqrt(pow(r,2)+pow(g,2)+pow(b,2)) - sqrt(pow(127,2)+pow(127,2)+pow(127,2)));
      c.c = col;
    }
    println("final color weight: "+weightColor);
  }while(abs(weightColor) > epsilonColor);
  
  background(bg);
  for (Circle c : circles){
    fill(c.c);
    circle(c.x,c.y,pointSize);
  }
  println("Position tries: "+triesPosition);
  println("Color tries: "+triesColor);
  Runtime.getRuntime().addShutdownHook(new Thread() {
      public void run(){
        String svgContent;
        try{
          svgContent = Files.readString(Paths.get("/home/user/sketchbook/balancednp/sketch_210806b/balanced_np-"+timestamp+".svg"), StandardCharsets.US_ASCII);
        }catch(Exception e){
          throw new RuntimeException(e);
        }
        JSONObject json = new JSONObject();
        json.put("name", "Abstract Balance Study #0");
        json.put("description","Abstract Balance Study tries to generate aesthetically pleasing artworks by achieving balance of its elements.\n\n"+
        "On a grid of (n-1)x(n-1) elements, random combinations are generated until they are balanced by position and color.\n\n"+
        "Each circle adds a weight equal to its position on the grid, relative to the center.\n\n"+
        "Circles are considered balanced by position if the sum of their weights is zero.\n\n"+
        "A color adds a weight equal to its distance from the center of the rgb color space, which is set at (127, 127, 127).\n\n"+
        "Colors are considerd balanced if the sum of their weights is zero.\n\n"+
        "This study explores different possibilities within this arbitrary set of rules.\n"
        );
        json.put("image_data", svgContent);
        JSONArray attributes = new JSONArray();
        attributes.setJSONObject(0, makeAttribute("Position Tries", String.valueOf(triesPosition)));
        attributes.setJSONObject(1, makeAttribute("Color Tries", String.valueOf(triesColor)));
        attributes.setJSONObject(2, makeAttribute("n", String.valueOf(currentDen)));
        json.put("attributes", attributes);
        saveJSONObject(json, "./metadata-"+timestamp+".json");
      }
    }
  );
  exit();
}

JSONObject makeAttribute(String trait_type, String value){
  JSONObject attr = new JSONObject();
  attr.put("trait_type", trait_type);
  attr.put("value", value);
  return attr;
}
  
